import numpy as np
import math
import copy as copy
from scipy import interpolate
from scipy.linalg import lstsq
import time
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.mplot3d import Axes3D

tic = time.time()

filename = 't-meteo-grid.txt'
# filename = 't-meteo-grid-2.txt'
# filename = 't-meteo-grid-3.txt'
# filename = 't-meteo-grid-4.txt'
# filename = 'test.txt'
info_matrix = np.matrix([])


# ======================================================================================================================
"""
    m : int
        Количество строк сетки.
    n : int
        Количество столбцов сетки.   
"""
def dist_calc(m: int, n: int) -> tuple:
    rad = 6372795

    delta_x = 2 * math.pi / n
    delta_y = math.pi / (m + 1)
    x = np.array([])
    y = rad * delta_y

    # TODO придумать как высчитать расстояние между соседними точками для широт => COMPLETE
    for i in range(m):
        alfa = math.pi / 2. - (i + 1) * delta_y
        d = math.sin(alfa) * math.sin(alfa) + math.cos(alfa) * math.cos(alfa) * math.cos(delta_x)
        x = np.append(x, rad * math.acos(d))

    return x, y
# ======================================================================================================================


# ======================================================================================================================
"""
    file_name: str
        Полный путь к файлу для считывания.
"""
def read_matrix(file_name: str) -> np.matrix:
    with open(file_name, 'r') as file:
        info = np.array([line.rstrip().split('; ') for line in file])
        matrix = np.matrix(info).astype(float)

    return matrix
# ======================================================================================================================


# ======================================================================================================================
"""
    number: int, optional
        Количество добавочных точек на сторону.
    kind: {'all', 'center'}, optional
        Тип возвращаемого списка координат.
        Default is 'all'.
    
    Вычисляем относительные координаты точек для добавления.
"""
def additional_points_coordinates(number=1, kind='all') -> list:
    mass = list()
    rng = np.linspace(0, 1, (number + 2))

    if kind == 'all':
        for i in rng:
            for j in rng:
                if ((i == 0) & ((j == 0) | (j == 1))) | ((i == 1) & ((j == 0) | (j == 1))):
                    continue
                else:
                    mass.append((i, j))

    elif kind == 'center':
        for i in rng[1:-1]:
            for j in rng[1:-1]:
                mass.append((i, j))

    else:
        ValueError("Unsupported interpolation type.")

    return mass
# ======================================================================================================================


# ======================================================================================================================
"""
    source_points : np.matrix
        Входная матрица температур, которую хотим проинтерполировать.
    new_count: int, optional
        Новое число точек, которое хотим получить на одной стороне ячейки.
        Default is 3.
"""
def bilinear_interpolation(source_points: np.matrix, new_count=3) -> np.matrix:
    def bilin_interp(source_matrix: np.matrix, point: tuple) -> float:
        x, y = point

        return np.matrix([(1 - x), x]) @ source_matrix @ np.matrix([[1 - y], [y]])

    def interpolation(matr: np.matrix) -> np.matrix:
        m, n = matr.shape

        cell_len = new_count
        xv = yv = np.linspace(0, 1, num=cell_len)
        # перебор строк
        for i in range(0, m-1):
            m_res = np.matrix(np.zeros((cell_len, cell_len)))
            # перебор столбцов
            for j in range(0, n-1):
                m_m = matr[i:(i + 2), j:(j + 2)]

                for p in range(cell_len):
                    for q in range(cell_len):
                        m_res[p, q] = bilin_interp(m_m, (xv[p], yv[q]))

                if j > 0:
                    h_res = np.hstack((h_res, m_res[:, 1:]))
                else:
                    h_res = copy.deepcopy(m_res)

            if i > 0:
                res = np.vstack((res, h_res[1:, :]))
            else:
                res = copy.deepcopy(h_res)
        return res

    return interpolation(source_points)
# ======================================================================================================================


# ======================================================================================================================
"""
    source_points : np.matrix
        Входная матрица температур, которую хотим проинтерполировать.
    new_count: int, optional
        Новое число точек, которое хотим получить на одной стороне ячейки.
        Default is 3.
"""
def bicubic_interpolation(source_points: np.matrix, new_count=3) -> np.matrix:
    def prepare_data(matr=source_points):
        res = np.vstack((2 * matr[0, :] - matr[1, :], matr, 2 * matr[-1, :] - matr[-2, :]))

        return np.hstack((res[:, -1], res, res[:, 0]))

    def bicub_interp(source_matrix: np.matrix, point: tuple) -> float:
        x, y = point

        def cubic_interpolation(p: np.array, m_x: float) -> float:
            res = p[1] + (-0.5 * p[0] + 0.5 * p[2]) * m_x + (p[0] - 2.5 * p[1] + 2.0 * p[2] - 0.5 * p[3]) * m_x * m_x \
                  + (-0.5 * p[0] + 1.5 * p[1] - 1.5 * p[2] + 0.5 * p[3]) * m_x * m_x * m_x
            return res

        tmp = list()
        tmp.append(cubic_interpolation(source_matrix[0].A1, x))
        tmp.append(cubic_interpolation(source_matrix[1].A1, x))
        tmp.append(cubic_interpolation(source_matrix[2].A1, x))
        tmp.append(cubic_interpolation(source_matrix[3].A1, x))
        tmp = np.array(tmp)

        return cubic_interpolation(tmp, y)

    def interpolation(matr: np.matrix) -> np.matrix:
        m, n = matr.shape
        m_matr = prepare_data(matr)

        cell_len = new_count
        xv = yv = np.linspace(0, 1, num=cell_len)
        # перебор строк
        for i in range(1, m):
            m_res = np.matrix(np.zeros((cell_len, cell_len)))
            # перебор столбцов
            for j in range(1, n):
                m_m = m_matr[(i - 1):(i + 3), (j - 1):(j + 3)]

                for p in range(cell_len):
                    for q in range(cell_len):
                        m_res[q, p] = bicub_interp(m_m, (xv[p], yv[q]))

                if j > 1:
                    h_res = np.hstack((h_res, m_res[:, 1:]))
                else:
                    h_res = copy.deepcopy(m_res)

            if i > 1:
                res = np.vstack((res, h_res[1:, :]))
            else:
                res = copy.deepcopy(h_res)
        return res

    return interpolation(source_points)
# ======================================================================================================================


# ======================================================================================================================
# TODO реализовать ускоренный метод подсчёта  => COMPLETE
"""
    source_points : np.matrix
        Входная матрица температур, которую хотим проинтерполировать.
    new_count: int, optional
        Новое число точек, которое хотим получить на одной стороне ячейки.
        Default is 3.
"""
def fast_bicubic_interpolation(source_points: np.matrix, new_count=3) -> np.matrix:
    def update_coefficients(p=source_points) -> np.matrix:
        """
              Следует отметить, что такой способ обеспечивает непрерывность самой функции и её второй производной на
            границах ячеек, но приводит к разрыву первых производных на границах ячеек. Для обеспечения непрерывности
            самой функции и её первой производной необходимо подставлять в исх-ое выражение значения функции и значения
            первых производных по направлениям x и y в вершинах центральной ячейки, производные рассчитываются через
            центральные разности.
              Для подстановки производных выражение должно быть продифферинцировано соответствующим образом.
        """

        #               1     2    3    4    5    6     7    8    9   10   11   12   13   14   15   16
        M = np.matrix([[ 0,   0,   0,   0,   0,   36,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
                       [ 0, -12,   0,   0,   0,  -18,   0,   0,   0,  36,   0,   0,   0,  -6,   0,   0],
                       [ 0,  18,   0,   0,   0,  -36,   0,   0,   0,  18,   0,   0,   0,   0,   0,   0],
                       [ 0,  -6,   0,   0,   0,   18,   0,   0,   0, -18,   0,   0,   0,   6,   0,   0],
                       [ 0,   0,   0,   0, -12,  -18,  36,  -6,   0,   0,   0,   0,   0,   0,   0,   0],
                       [ 4,   6, -12,   2,   6,    9, -18,   3, -12, -18,  36,  -6,   2,   3,  -6,   1],
                       [-6,  -9,  18,  -3,  12,   18, -36,   6,  -6,  -9,  18,  -3,   0,   0,   0,   0],
                       [ 2,   3,  -6,   1,  -6,   -9,  18,  -3,   6,   9, -18,   3,  -2,  -3,   6,  -1],
                       [ 0,   0,   0,   0,  18,  -36,  18,   0,   0,   0,   0,   0,   0,   0,   0,   0],
                       [-6,  12,  -6,   0,  -9,   18,  -9,   0,  18, -36,  18,   0,  -3,   6,  -3,   0],
                       [ 9, -18,   9,   0, -18,   36, -18,   0,   9, -18,   9,   0,   0,   0,   0,   0],
                       [-3,   6,  -3,   0,   9,  -18,   9,   0,  -9,  18,  -9,   0,   3,  -6,   3,   0],
                       [ 0,   0,   0,   0,  -6,   18, -18,   6,   0,   0,   0,   0,   0,   0,   0,   0],
                       [ 2,  -6,   6,  -2,   3,   -9,   9,  -3,  -6,  18, -18,   6,   1,  -3,   3,  -1],
                       [-3,   9,  -9,   3,   6,  -18,  18,  -6,  -3,   9,  -9,   3,   0,   0,   0,   0],
                       [ 1,  -3,   3,  -1,  -3,    9,  -9,   3,   3,  -9,   9,  -3,  -1,   3,  -3,   1]])

        return (1/36 * M @ p.A1.T).reshape((4, 4))

    def prepare_data(matr=source_points):
        res = np.vstack((2 * matr[0, :] - matr[1, :], matr, 2 * matr[-1, :] - matr[-2, :]))

        return np.hstack((res[:, -1], res, res[:, 0]))

    def get_value(a: np.matrix, point: tuple) -> float:
        x, y = point
        x2 = x * x
        x3 = x2 * x
        y2 = y * y
        y3 = y2 * y

        res = (a[0, 0] + a[0, 1] * y + a[0, 2] * y2 + a[0, 3] * y3) + \
              (a[1, 0] + a[1, 1] * y + a[1, 2] * y2 + a[1, 3] * y3) * x + \
              (a[2, 0] + a[2, 1] * y + a[2, 2] * y2 + a[2, 3] * y3) * x2 + \
              (a[3, 0] + a[3, 1] * y + a[2, 2] * y2 + a[3, 3] * y3) * x3

        return res

    def interpolation(matr: np.matrix) -> np.matrix:
        m, n = matr.shape
        m_matr = prepare_data(matr)

        cell_len = new_count
        xv = yv = np.linspace(0, 1, num=cell_len)
        # перебор строк
        for i in range(1, m):
            m_res = np.matrix(np.zeros((cell_len, cell_len)))
            # перебор столбцов
            for j in range(1, n):
                m_m = m_matr[(i - 1):(i + 3), (j - 1):(j + 3)]
                coef_matr = update_coefficients(m_m)

                for p in range(cell_len):
                    for q in range(cell_len):
                        m_res[q, p] = get_value(coef_matr, (xv[p], yv[q]))

                if j > 1:
                    h_res = np.hstack((h_res, m_res[:, 1:]))
                else:
                    h_res = copy.deepcopy(m_res)

            if i > 1:
                res = np.vstack((res, h_res[1:, :]))
            else:
                res = copy.deepcopy(h_res)

        return res

    return interpolation(source_points)
# ======================================================================================================================


# ======================================================================================================================
# TODO реализовать интерполяцию сплайнами  => COMPLETE
"""
    source_points : np.matrix
        Входная матрица температур, которую хотим проинтерполировать.
    new_count: int, optional
        Новое число точек, которое хотим получить на одной стороне ячейки.
        Default is 3.
    kind: {'cubic', 'cubic_B_splines', 'akima_splines', 'smooth_splines', 'rbf' }, optional
        Тип интерполяции сплайнами.
        Default is 'cubic'.   
"""
def spline_interpolation(source_points: np.matrix, new_count=3, kind='cubic') -> np.matrix:
    def get_step() -> float:
        return 1 / (new_count - 1)

    def cubic_interp_spline_surface():
        def cubic_spline(points: np.array, step: int, direction: str) -> np.matrix:
            n = points.size - 1
            res = np.matrix(np.zeros((n, 4)))

            a = copy.deepcopy(points[1:])
            b = np.array(np.zeros(n))
            c = np.array(np.zeros(n))
            d = np.array(np.zeros(n))

            f = lambda x: (points[(n + x + 1) % n] - points[(n + x) % n]) / step

            # помнить про то, что индексация матрицы начинается с 0, а индексы коэффициентов с 1
            left = np.matrix(np.zeros((n - 1, n)))
            right = 6 / step * np.array(np.ones(n - 1))
            for i in range(2, n + 1):
                if i < n:
                    left[i - 2, i - 1] = 1
                left[i - 2, i - 2] = 4
                if i > 2:
                    left[i - 2, i - 3] = 1
                right[i - 2] *= f(i - 2) - f(i - 1)
            right = right.reshape((n - 1, 1))
            c = lstsq(left, right)[0]

            # для широты переодические ГУ
            if direction == 'latitude':
                for i in range(2, n + 1):
                    b[i - 1] = f(i - 1) + step * (c[i - 1] / 3 + c[i - 2] / 6)
                    d[i - 1] = (c[i - 1] - c[i - 2]) / step
                b[0] = f(0) + step * (c[0] / 3 + c[-1] / 6)
                # b[0] = b[-1] + step * (c[-1] + step*d[-1]/2)
                d[0] = d[-1]

                res = np.column_stack((a, b, c, d))

            # для долготы естественные ГУ
            elif direction == 'longitude':
                for i in range(2, n + 1):
                    b[i - 1] = f(i - 1) + step * (c[i - 1] / 3 + c[i - 2] / 6)
                    d[i - 1] = (c[i - 1] - c[i - 2]) / step
                b[0] = f(0) + step * (c[0] / 3)
                d[0] = (c[0]) / step

                res = np.column_stack((a, b, c, d))

            return res

        def get_value(z: np.matrix, bx: np.matrix, by: np.matrix, bxy: np.matrix, point: tuple) -> float:
            f1 = lambda x: (1 - x) * (1 - x) * (1 + 2 * x)
            f2 = lambda x: x * x * (3 - 2 * x)
            f3 = lambda x: x * (1 - x) * (1 - x)
            f4 = lambda x: -x * x * (1 - x)

            t, u = point
            a = np.matrix([f1(t), f2(t), f3(t), f4(t)])
            b = np.matrix([f1(u), f2(u), f3(u), f4(u)]).T
            M = np.vstack((np.hstack((z, bx)), np.hstack((by, bxy))))

            return (a @ M @ b)

        m, n = source_points.shape
        x_dist, y_dist = dist_calc(source_points.shape[0], source_points.shape[1])
        tmp_x_dist = np.hstack((x_dist[0], x_dist, x_dist[-1]))

        work_matr = copy.deepcopy(source_points)
        work_matr = np.vstack((work_matr[1] - work_matr[0], work_matr, work_matr[-1] - work_matr[-2]))
        work_matr = work_matr[::-1]

        for i in range(m + 2):
            if i > 0:
                bx = np.vstack((bx, cubic_spline(work_matr[i].A1, tmp_x_dist[i], 'latitude')[:, 1]))
            else:
                bx = cubic_spline(work_matr[i].A1, tmp_x_dist[i], 'latitude')[:, 1]
        bx = np.column_stack((bx, bx[:, 0]))

        for j in range(n):
            if j > 0:
                by = np.column_stack((by, cubic_spline(work_matr[:, j].A1, y_dist, 'longitude')[:, 1]))

                bxy = np.column_stack((bxy, cubic_spline(bx[:, j], y_dist, 'longitude')[:, 1]))
            else:
                by = cubic_spline(work_matr[:, j].A1, y_dist, 'longitude')[:, 1]

                bxy = cubic_spline(bx[:, j], y_dist, 'longitude')[:, 1]

        bx, by, bxy = map(lambda x: np.matrix(x), (bx, by, bxy))
        bx, by, bxy = map(lambda x: x[1: -1], (bx, by, bxy))
        bx, by, bxy = map(lambda x: x[::-1], (bx, by, bxy))
        by, bxy = map(lambda x: np.vstack((x, x[0])), (by, bxy))

        def interpolation():
            mm, nn = map(lambda x: x + (new_count - 2) * (x - 1), [m, n])
            step = get_step()

            interp_matr = np.matrix(np.zeros((mm, nn)))
            add_p = additional_points_coordinates(new_count - 2)
            for i in range(m-1):
                ii = (new_count - 1) * i
                for j in range(n-1):
                    jj = (new_count - 1) * j
                    interp_matr[ii, jj] = source_points[i, j]
                    for el in add_p:
                        elx, ely = el
                        elx, ely = map(lambda x: int(round(x / step)), (elx, ely))
                        interp_matr[ii + elx, jj + ely] = get_value(source_points[i:(i+2), j:(j+2)],
                                                                    bx[i:(i+2), j:(j+2)],
                                                                    by[i:(i+2), j:(j+2)],
                                                                    bxy[i:(i+2), j:(j+2)],
                                                                    el)
                interp_matr[ii, -1] = source_points[i, -1]

            for k in range(n):
                interp_matr[-1, k * (new_count - 1)] = source_points[-1, k]

            return interp_matr

        return interpolation()

    # TODO scipy.interpolate.interp2d и это работает => COMPLETE
    def cubic_B_spline_interpolate_():
        m, n = source_points.shape

        x, y = np.meshgrid(np.arange(n), np.arange(m))
        test = interpolate.bisplrep(x.flat, y.flat, source_points.A1, s=(m*n/4.95))
        step = get_step()
        xnew = np.arange(0, m - 0.9999, step)
        ynew = np.arange(0, n - 0.9999, step)
        mm, nn = map(lambda x: x + (new_count - 2) * (x - 1), source_points.shape)

        m = n = 0
        matrix = np.matrix(np.zeros((mm, nn)))
        for i in xnew:
            for j in ynew:
                matrix[m, n] = interpolate.bisplev(j, i, test)
                n += 1
            m += 1
            n = 0

        return matrix
		
    def cubic_B_spline_interpolate():
        m, n = source_points.shape

        test = interpolate.interp2d(np.arange(n), np.arange(m), source_points, kind='cubic')
        step = get_step()
        xnew = np.arange(0, m - 0.9999, step)
        ynew = np.arange(0, n - 0.9999, step)
        mm, nn = map(lambda x: x + (new_count - 2) * (x - 1), source_points.shape)

        m = n = 0
        matrix = np.matrix(np.zeros((mm, nn)))
        for i in xnew:
            for j in ynew:
                matrix[m, n] = test(j, i)
                n += 1
            m += 1
            n = 0

        return matrix

    def akima_spline_interpolate():
        def akima_spline(points: np.array, step: int, direction: str) -> np.matrix:
            n = points.size - 1
            res = np.matrix(np.zeros((n, 4)))

            a = copy.deepcopy(points[1:])
            b = np.array(np.zeros(n))
            c = np.array(np.zeros(n))
            d = np.array(np.zeros(n))

            m = np.diff(points) / step
            """
                В этом массиве содержатся элементы m[-2], m[-1], m[n], m[n+1].
                Они будут использованы в дальнейшем, но могут быть вычислены в m.            
            """
            _m = np.array(np.zeros(4))
            tl = tr = np.array(np.zeros(n + 1))

            # для широты переодические ГУ
            if direction == 'latitude':
                _m[0] = m[-2]
                _m[1] = m[-1]
                _m[2] = 2 * m[0]
                _m[3] = 3 * m[1]

            # для долготы естественные ГУ
            elif direction == 'longitude':
                _m[0] = 3 * m[0] - 2 * m[1]
                _m[1] = 2 * m[0] - 2 * m[1]
                _m[2] = 2 * m[-1] - 2 * m[-2]
                _m[3] = 3 * m[-1] - 2 * m[-2]

            m1 = np.concatenate((_m[:2], m, _m[2:]))

            # TODO подумать и выбрать реализацию (либо то, что в комментах, либо то, что сейчас используется) => COMPLETE +-

            dm = np.abs(np.diff(m1))
            f1 = dm[2:(n + 3)]
            f2 = dm[0:(n + 1)]
            f12 = f1 + f2

            ids = np.nonzero(f12 > 1e-9 * np.max(f12))[0]
            b = m1[1:(n + 2)]

            b[ids] = (f1[ids] * m1[ids + 1] + f2[ids] * m1[ids + 2]) / f12[ids]
            c = (3.0 * m - 2.0 * b[0:n] - b[1:(n + 1)]) / step
            d = (b[0:n] + b[1:(n + 1)] - 2.0 * m) / step ** 2

            res = np.column_stack((a, b[:-1], c * 2, d * 6))

            """
                for i in range(n+1):
                    ne = np.abs(m1[i+3] - m1[i+2]) + np.abs(m1[i+1] - m1[i])
                    if ne > 0:
                        tl[i] = m1[i+1] + np.abs(m1[i+1] - m1[i])*(m1[i+2] - m1[i+1])/ne
                        tr[i] = tl[i]
                    else:
                        tl[i] = m1[i+1]
                        tr[i] = m1[i+2]

                b = tr
                c = (3*m - 2*tr[:-1] - tl[1:]) / step
                d = (tr[:-1] + tl[1:] - 2*m) / (step**2)
                res = np.column_stack((a, b[:-1], c * 2, d * 6))
            """

            return res

        def get_value(z: np.matrix, bx: np.matrix, by: np.matrix, bxy: np.matrix, point: tuple) -> float:
            f1 = lambda x: (1 - x) * (1 - x) * (1 + 2 * x)
            f2 = lambda x: x * x * (3 - 2 * x)
            f3 = lambda x: x * (1 - x) * (1 - x)
            f4 = lambda x: -x * x * (1 - x)

            t, u = point
            a = np.matrix([f1(t), f2(t), f3(t), f4(t)])
            b = np.matrix([f1(u), f2(u), f3(u), f4(u)]).T
            M = np.vstack((np.hstack((z, bx)), np.hstack((by, bxy))))

            return (a @ M @ b)

        m, n = source_points.shape
        x_dist, y_dist = dist_calc(source_points.shape[0], source_points.shape[1])
        tmp_x_dist = np.hstack((x_dist[0], x_dist, x_dist[-1]))

        work_matr = copy.deepcopy(source_points)
        work_matr = np.vstack((work_matr[1] - work_matr[0], work_matr, work_matr[-1] - work_matr[-2]))
        work_matr = work_matr[::-1]

        for i in range(m + 2):
            if i > 0:
                bx = np.vstack((bx, akima_spline(work_matr[i].A1, tmp_x_dist[i], 'latitude')[:, 1]))
                # bx = np.vstack((bx, akima_spline(work_matr[i].A1, 1, 'latitude')[:, 1]))
            else:
                bx = akima_spline(work_matr[i].A1, tmp_x_dist[i], 'latitude')[:, 1]
                # bx = akima_spline(work_matr[i].A1, 1, 'latitude')[:, 1]
        bx = np.column_stack((bx, bx[:, 0]))

        for j in range(n):
            if j > 0:
                by = np.column_stack((by, akima_spline(work_matr[:, j].A1, y_dist, 'longitude')[:, 1]))
                # by = np.column_stack((by, akima_spline(work_matr[:, j].A1, 1, 'longitude')[:, 1]))

                bxy = np.column_stack((bxy, akima_spline(bx[:, j], y_dist, 'longitude')[:, 1]))
                # bxy = np.column_stack((bxy, akima_spline(bx[:, j], 1, 'longitude')[:, 1]))
            else:
                by = akima_spline(work_matr[:, j].A1, y_dist, 'longitude')[:, 1]
                # by = akima_spline(work_matr[:, j].A1, 1, 'longitude')[:, 1]

                bxy = akima_spline(bx[:, j], y_dist, 'longitude')[:, 1]
                # bxy = akima_spline(bx[:, j], 1, 'longitude')[:, 1]

        bx, by, bxy = map(lambda x: np.matrix(x), (bx, by, bxy))
        bx, by, bxy = map(lambda x: x[1: -1], (bx, by, bxy))
        bx, by, bxy = map(lambda x: x[::-1], (bx, by, bxy))
        by, bxy = map(lambda x: np.vstack((x, x[0])), (by, bxy))

        def interpolation():
            mm, nn = map(lambda x: x + (new_count - 2) * (x - 1), [m, n])
            step = get_step()

            interp_matr = np.matrix(np.zeros((mm, nn)))
            add_p = additional_points_coordinates(new_count - 2)
            for i in range(m - 1):
                ii = (new_count - 1) * i
                for j in range(n - 1):
                    jj = (new_count - 1) * j
                    interp_matr[ii, jj] = source_points[i, j]
                    for el in add_p:
                        elx, ely = el
                        elx, ely = map(lambda x: int(round(x / step)), (elx, ely))
                        interp_matr[ii + elx, jj + ely] = get_value(source_points[i:(i + 2), j:(j + 2)],
                                                                    bx[i:(i + 2), j:(j + 2)],
                                                                    by[i:(i + 2), j:(j + 2)],
                                                                    bxy[i:(i + 2), j:(j + 2)],
                                                                    el)
                interp_matr[ii, -1] = source_points[i, -1]

            for k in range(n):
                interp_matr[-1, k * (new_count - 1)] = source_points[-1, k]

            return interp_matr

        return interpolation()

    def rbf_interpolate():
        m, n = source_points.shape

        x, y = np.meshgrid(np.arange(n), np.arange(m))
        # TODO прочекать результаты при различных заданиях базисной функции RBF
        rbfi = interpolate.Rbf(x, y, source_points.A1, function='multiquadric')
        step = get_step()
        xnew = np.arange(0, m - 0.9999, step)
        ynew = np.arange(0, n - 0.9999, step)
        mm, nn = map(lambda x: x + (new_count - 2) * (x - 1), source_points.shape)

        m = n = 0
        matrix = np.matrix(np.zeros((mm, nn)))
        for i in xnew:
            for j in ynew:
                matrix[m, n] = rbfi(j, i)
                n += 1
            m += 1
            n = 0

        return matrix

    def smooth_spline_surface():
        m, n = source_points.shape

        x, y = np.meshgrid(np.arange(n), np.arange(m))
        # TODO прочекать результаты при различных заданиях аргумента s => COMPLETE (под каждую сетку необходимо отдельным образом подбирать)
        sbsp = interpolate.SmoothBivariateSpline(x.flat, y.flat, source_points.A1, s=m*n*1.5)
        step = get_step()
        xnew = np.arange(0, m - 0.9999, step)
        ynew = np.arange(0, n - 0.9999, step)
        mm, nn = map(lambda x: x + (new_count - 2) * (x - 1), source_points.shape)

        m = n = 0
        matrix = np.matrix(np.zeros((mm, nn)))
        for i in xnew:
            for j in ynew:
                matrix[m, n] = sbsp(j, i)
                n += 1
            m += 1
            n = 0

        return matrix

    if kind == 'cubic':
        return cubic_interp_spline_surface()
    elif kind == 'cubic_B_splines':
        return cubic_B_spline_interpolate()
    elif kind == 'cubic_B_splines_':
        return cubic_B_spline_interpolate_()
    elif kind == 'akima_splines':
        return akima_spline_interpolate()
    elif kind == 'rbf':
        return rbf_interpolate()
    elif kind == 'smooth_splines':
        return smooth_spline_surface()
    else:
        raise ValueError('Incorrect function arguments')
# ======================================================================================================================


# ======================================================================================================================
# TODO всё говно - переделать => COMPLETE
"""
    source_points : np.matrix
        Входная матрица температур, которую хотим проинтерполировать.
    new_count: int, optional
        Новое число точек, которое хотим получить на одной стороне ячейки.
        Default is 3.
    
    Используем поверхность Безье 3-го порядка
"""
def bezier_interpolation(source_points: np.matrix, new_count=3) -> np.matrix:
    dim = 3

    def prepare_data(matr=source_points, add_rows=2, add_cols=2):
        if add_rows == 0:
            if add_cols == 0:
                return matr
            elif add_cols == 1:
                return np.hstack((matr, matr[:, 0]))
            elif add_cols == 2:
                return np.hstack((matr, matr[:, 0:2]))

        # экстраполируем сетку на одну строку вверх
        elif add_rows == 1:
            res = np.vstack((2 * matr[0, :] - matr[1, :], matr))
            if add_cols == 0:
                return res
            elif add_cols == 1:
                return np.hstack((res, res[:, 0]))
            elif add_cols == 2:
                return np.hstack((res, res[:, 0:2]))

        # экстраполируем сетку на одну строку вверх и вниз
        elif add_rows == 2:
            res = np.vstack((2 * matr[0, :] - matr[1, :], matr, 2 * matr[-1, :] - matr[-2, :]))
            if add_cols == 0:
                return res
            elif add_cols == 1:
                return np.hstack((res, res[:, 0]))
            elif add_cols == 2:
                return np.hstack((res, res[:, 0:2]))

    def bezier_curve(points: np.array, t: float) -> float:
        b0 = (1 - t) * (1 - t) * (1 - t)
        b1 = 3 * t * (1 - t) * (1 - t)
        b2 = 3 * t * t * (1 - t)
        b3 = t * t * t

        return points[0] * b0 + points[1] * b1 + points[2] * b2 + points[3] * b3

    def bezier_surface(points: np.array, additional_point: tuple) -> float:
        u, v = additional_point

        curve = np.array(np.zeros(4))
        for i in range(dim + 1):
            curve_u = np.array(np.zeros(4))
            curve_u[0] = points[i * 4]
            curve_u[1] = points[i * 4 + 1]
            curve_u[2] = points[i * 4 + 2]
            curve_u[3] = points[i * 4 + 3]
            curve[i] = bezier_curve(curve_u, u)

        return bezier_curve(curve, v)

    def interpolation(matr: np.matrix) -> np.matrix:
        m, n = matr.shape
        mm, nn = map(lambda xx: 3 - ((xx - 4) % 3), (m, n))
        m_matr = prepare_data(matr, add_rows=mm, add_cols=nn)

        cell_len = 3 * (new_count - 2) + 4
        xv = yv = np.linspace(0, 1, num=cell_len)
        # перебор строк
        for i in range(1, m, 3):
            m_res = np.matrix(np.zeros((cell_len, cell_len)))
            # перебор столбцов
            for j in range(1, n, 3):
                m_m = m_matr[(i - 1):(i + 3), (j - 1):(j + 3)]
                for p in range(cell_len):
                    for q in range(cell_len):
                        m_res[q, p] = bezier_surface(m_m.A1, (xv[p], yv[q]))

                if j > 1:
                    h_res = np.hstack((h_res, m_res[:, 1:]))
                else:
                    h_res = copy.deepcopy(m_res)

            if i > 1:
                res = np.vstack((res, h_res[1:, :]))
            else:
                res = copy.deepcopy(h_res)

        if mm == 0:
            if nn == 0:
                return res[:, :]
            elif nn == 1:
                return res[:, :(1 - new_count)]
            elif nn == 2:
                return res[:, :(2 * (1 - new_count))]

        elif mm == 1:
            if nn == 0:
                return res[(new_count - 1):, :]
            elif nn == 1:
                return res[(new_count - 1):, :(1 - new_count)]
            elif nn == 2:
                return res[(new_count - 1):, :(2 * (1 - new_count))]

        elif mm == 2:
            if nn == 0:
                return res[(new_count - 1):(1 - new_count), :]
            elif nn == 1:
                return res[(new_count - 1):(1 - new_count), :(1 - new_count)]
            elif nn == 2:
                return res[(new_count - 1):(1 - new_count), :(2 * (1 - new_count))]

    return interpolation(source_points)
# ======================================================================================================================


# ======================================================================================================================
"""
    source_points : np.matrix
        Входная матрица температур, которую хотим проинтерполировать.
    kind : { 'bilinear', 'bicubic', 'cubic_splines', 'akima_splines' }, optional    
        Тип интерполяции, кросс-валидацию которой хотим провести.
        Default is 'bicubic'.
    
    Проводит интерполирование в исходных точках для кросс-валидации
"""
def cross_validation(source_points: np.matrix, kind='bicubic') -> np.matrix:
    def bilin_validation() -> np.matrix:
        def bilin_interp(source_matrix: np.matrix, point: tuple) -> float:
            x, y = point

            return np.matrix([(1 - x), x]) @ source_matrix @ np.matrix([[1 - y], [y]])

        def get_matr(matrix: np.matrix, coord: tuple) -> np.matrix:
            x, y = coord
            res = matrix[x:(x + 4), y:(y + 4)]

            return res[::2, ::2]

        def add_value(orig: np.array, value: float) -> np.array:
            if orig is not None:
                return np.append(orig, value)
            else:
                return np.array(value)

        m, n = source_points.shape
        res = np.empty((m, n), dtype=object)

        # перебор строк
        for i in range(m - 2):
            # перебор столбцов
            for j in range(n - 2):
                m_m = get_matr(source_points, (i, j))

                res[i, j + 1] = add_value(res[i, j + 1], bilin_interp(m_m, (0, 0.5)))
                res[i + 1, j] = add_value(res[i + 1, j], bilin_interp(m_m, (0.5, 0)))
                res[i + 1, j + 1] = add_value(res[i + 1, j + 1], bilin_interp(m_m, (0.5, 0.5)))
                res[i + 1, j + 2] = add_value(res[i + 1, j + 2], bilin_interp(m_m, (0.5, 1)))
                res[i + 2, j + 1] = add_value(res[i + 2, j + 1], bilin_interp(m_m, (1, 0.5)))

        return res

    def bicub_validation() -> np.matrix:
        def bicub_interp(source_matrix: np.matrix, point: tuple) -> float:
            x, y = point

            def cubic_interpolation(p: np.array, m_x: float) -> float:
                res = p[1] + (-0.5 * p[0] + 0.5 * p[2]) * m_x + (p[0] - 2.5 * p[1] + 2.0 * p[2] - 0.5 * p[3]) * m_x * m_x \
                      + (-0.5 * p[0] + 1.5 * p[1] - 1.5 * p[2] + 0.5 * p[3]) * m_x * m_x * m_x
                return res

            tmp = list()
            tmp.append(cubic_interpolation(source_matrix[0].A1, x))
            tmp.append(cubic_interpolation(source_matrix[1].A1, x))
            tmp.append(cubic_interpolation(source_matrix[2].A1, x))
            tmp.append(cubic_interpolation(source_matrix[3].A1, x))
            tmp = np.array(tmp)

            return cubic_interpolation(tmp, y)
        
        def get_matr(matrix: np.matrix, coord: tuple) -> np.matrix:
            x, y = coord
            res = matrix[x:(x+8), y:(y+8)]
            
            return res[::2, ::2]
        
        def add_value(orig: np.array, value: float) -> np.array:
            if orig is not None:
                return np.append(orig, value)
            else:
                return np.array([value])
        
        matr = np.vstack((2 * source_points[0, :] - source_points[1, :], source_points,
                          2 * source_points[-1, :] - source_points[-2, :]))
        matr = np.hstack((matr[:, -1], matr, matr[:, 0]))
        
        m, n = source_points.shape
        res = np.empty((m, n), dtype=object)

        # перебор строк
        for i in range(m-4):
            # перебор столбцов
            for j in range(n-4):
                m_m = get_matr(matr, (i, j))

                res[i+1, j+2] = add_value(res[i+1, j+2], bicub_interp(m_m, (0.5, 0)))
                res[i+2, j+1] = add_value(res[i+2, j+1], bicub_interp(m_m, (0, 0.5)))
                res[i+2, j+2] = add_value(res[i+2, j+2], bicub_interp(m_m, (0.5, 0.5)))
                res[i+2, j+3] = add_value(res[i+2, j+3], bicub_interp(m_m, (1, 0.5)))
                res[i+3, j+2] = add_value(res[i+3, j+2], bicub_interp(m_m, (0.5, 1)))
                
        return res

    def cub_splines_validation() -> np.matrix:
        def add_value(orig: np.array, value: float) -> np.array:
            if orig is not None:
                return np.append(orig, value)
            else:
                return np.array(value)
        
        def cubic_spline(points: np.array, step: int, direction: str) -> np.matrix:
            n = points.size - 1
            res = np.matrix(np.zeros((n, 4)))

            a = copy.deepcopy(points[1:])
            b = np.array(np.zeros(n))
            c = np.array(np.zeros(n))
            d = np.array(np.zeros(n))

            f = lambda x: (points[(n + x + 1) % n] - points[(n + x) % n]) / step

            # помнить про то, что индексация матрицы начинается с 0, а индексы коэффициентов с 1
            left = np.matrix(np.zeros((n - 1, n)))
            right = 6 / step * np.array(np.ones(n - 1))
            for i in range(2, n + 1):
                if i < n:
                    left[i - 2, i - 1] = 1
                left[i - 2, i - 2] = 4
                if i > 2:
                    left[i - 2, i - 3] = 1
                right[i - 2] *= f(i - 2) - f(i - 1)
            right = right.reshape((n - 1, 1))
            c = lstsq(left, right)[0]

            # для широты переодические ГУ
            if direction == 'latitude':
                for i in range(2, n + 1):
                    b[i - 1] = f(i - 1) + step * (c[i - 1] / 3 + c[i - 2] / 6)
                    d[i - 1] = (c[i - 1] - c[i - 2]) / step
                b[0] = f(0) + step * (c[0] / 3 + c[-1] / 6)
                # b[0] = b[-1] + step * (c[-1] + step*d[-1]/2)
                d[0] = d[-1]

                res = np.column_stack((a, b, c, d))

            # для долготы естественные ГУ
            elif direction == 'longitude':
                for i in range(2, n + 1):
                    b[i - 1] = f(i - 1) + step * (c[i - 1] / 3 + c[i - 2] / 6)
                    d[i - 1] = (c[i - 1] - c[i - 2]) / step
                b[0] = f(0) + step * (c[0] / 3)
                d[0] = (c[0]) / step

                res = np.column_stack((a, b, c, d))

            return res

        def get_value(z: np.matrix, bx: np.matrix, by: np.matrix, bxy: np.matrix, point: tuple) -> float:
            f1 = lambda x: (1 - x) * (1 - x) * (1 + 2 * x)
            f2 = lambda x: x * x * (3 - 2 * x)
            f3 = lambda x: x * (1 - x) * (1 - x)
            f4 = lambda x: -x * x * (1 - x)

            t, u = point
            a = np.matrix([f1(t), f2(t), f3(t), f4(t)])
            b = np.matrix([f1(u), f2(u), f3(u), f4(u)]).T
            M = np.vstack((np.hstack((z, bx)), np.hstack((by, bxy))))

            return (a @ M @ b)

        m, n = source_points.shape
        x_dist, y_dist = dist_calc(source_points.shape[0], source_points.shape[1])
        x_dist, y_dist = map(lambda x: x*2, (x_dist, y_dist))
        res = np.empty((m, n), dtype=object)

        for k in range(4):
            if k == 0:
                work_matr = copy.deepcopy(source_points[::2, ::2])
                work_matr = np.vstack((work_matr[1] - work_matr[0], work_matr, work_matr[-1] - work_matr[-2]))
                work_matr = work_matr[::-1]
                tmp_x_dist = copy.deepcopy(x_dist[::2])
                tmp_x_dist = np.hstack((tmp_x_dist[0], tmp_x_dist, tmp_x_dist[-1]))
                di = dj = 0
                sp = copy.deepcopy(source_points[::2, ::2])

            elif k == 1:
                work_matr = copy.deepcopy(source_points[1::2, ::2])
                work_matr = np.vstack((work_matr[1] - work_matr[0], work_matr, work_matr[-1] - work_matr[-2]))
                work_matr = work_matr[::-1]
                tmp_x_dist = copy.deepcopy(x_dist[1::2])
                tmp_x_dist = np.hstack((tmp_x_dist[0], tmp_x_dist, tmp_x_dist[-1]))
                di = 1
                dj = 0
                sp = copy.deepcopy(source_points[1::2, ::2])

            elif k == 2:
                work_matr = copy.deepcopy(source_points[::2, 1::2])
                work_matr = np.vstack((work_matr[1] - work_matr[0], work_matr, work_matr[-1] - work_matr[-2]))
                work_matr = work_matr[::-1]
                tmp_x_dist = copy.deepcopy(x_dist[::2])
                tmp_x_dist = np.hstack((tmp_x_dist[0], tmp_x_dist, tmp_x_dist[-1]))
                di = 0
                dj = 1
                sp = copy.deepcopy(source_points[::2, 1::2])

            elif k == 3:
                work_matr = copy.deepcopy(source_points[1::2, 1::2])
                work_matr = np.vstack((work_matr[1] - work_matr[0], work_matr, work_matr[-1] - work_matr[-2]))
                work_matr = work_matr[::-1]
                tmp_x_dist = copy.deepcopy(x_dist[1::2])
                tmp_x_dist = np.hstack((tmp_x_dist[0], tmp_x_dist, tmp_x_dist[-1]))
                di = dj = 1
                sp = copy.deepcopy(source_points[1::2, 1::2])


            mm, nn = sp.shape
            for i in range(mm + 2):
                if i > 0:
                    bx = np.vstack((bx, cubic_spline(work_matr[i].A1, tmp_x_dist[i], 'latitude')[:, 1]))
                else:
                    bx = cubic_spline(work_matr[i].A1, tmp_x_dist[i], 'latitude')[:, 1]
            bx = np.column_stack((bx, bx[:, 0]))

            for j in range(nn):
                if j > 0:
                    by = np.column_stack((by, cubic_spline(work_matr[:, j].A1, y_dist, 'longitude')[:, 1]))

                    bxy = np.column_stack((bxy, cubic_spline(bx[:, j], y_dist, 'longitude')[:, 1]))
                else:
                    by = cubic_spline(work_matr[:, j].A1, y_dist, 'longitude')[:, 1]

                    bxy = cubic_spline(bx[:, j], y_dist, 'longitude')[:, 1]

            bx, by, bxy = map(lambda x: np.matrix(x), (bx, by, bxy))
            bx, by, bxy = map(lambda x: x[1: -1], (bx, by, bxy))
            bx, by, bxy = map(lambda x: x[::-1], (bx, by, bxy))
            by, bxy = map(lambda x: np.vstack((x, x[0])), (by, bxy))

            mm, nn = sp.shape

            # перебор строк
            for i in range(0, mm - 1):
                # перебор столбцов
                for j in range(0, nn - 1):
                    res[2*i + di, 2*j + dj + 1] = add_value(res[2*i + di, 2*j + dj + 1], get_value(sp[i:(i + 2), j:(j + 2)],
                                                                                           bx[i:(i + 2), j:(j + 2)],
                                                                                           by[i:(i + 2), j:(j + 2)],
                                                                                           bxy[i:(i + 2), j:(j + 2)],
                                                                                           (0, 0.5)))
                    res[2*i + di + 1, 2*j + dj] = add_value(res[2*i + di + 1, 2*j + dj], get_value(sp[i:(i + 2), j:(j + 2)],
                                                                                           bx[i:(i + 2), j:(j + 2)],
                                                                                           by[i:(i + 2), j:(j + 2)],
                                                                                           bxy[i:(i + 2), j:(j + 2)],
                                                                                           (0.5, 0)))
                    res[2*i + di + 1, 2*j + dj + 1] = add_value(res[2*i + di + 1, 2*j + dj + 1], get_value(sp[i:(i + 2), j:(j + 2)],
                                                                                                   bx[i:(i + 2), j:(j + 2)],
                                                                                                   by[i:(i + 2), j:(j + 2)],
                                                                                                   bxy[i:(i + 2), j:(j + 2)],
                                                                                                   (0.5, 0.5)))
                    res[2*i + di + 1, 2*j + dj + 2] = add_value(res[2*i + di + 1, 2*j + dj + 2], get_value(sp[i:(i + 2), j:(j + 2)],
                                                                                                   bx[i:(i + 2), j:(j + 2)],
                                                                                                   by[i:(i + 2), j:(j + 2)],
                                                                                                   bxy[i:(i + 2), j:(j + 2)],
                                                                                                   (0.5, 1)))
                    res[2*i + di + 2, 2*j + dj + 1] = add_value(res[2*i + di + 2, 2*j + dj + 1], get_value(sp[i:(i + 2), j:(j + 2)],
                                                                                                   bx[i:(i + 2), j:(j + 2)],
                                                                                                   by[i:(i + 2), j:(j + 2)],
                                                                                                   bxy[i:(i + 2), j:(j + 2)],
                                                                                                   (1, 0.5)))
            del(bx)
            del(by)
            del(bxy)

        return res

    def akima_splines_validation() -> np.matrix:
        def add_value(orig: np.array, value: float) -> np.array:
            if orig is not None:
                return np.append(orig, value)
            else:
                return np.array(value)
        
        def akima_spline(points: np.array, step: int, direction: str) -> np.matrix:
            n = points.size - 1
            res = np.matrix(np.zeros((n, 4)))

            a = copy.deepcopy(points[1:])
            b = np.array(np.zeros(n))
            c = np.array(np.zeros(n))
            d = np.array(np.zeros(n))

            m = np.diff(points) / step
            """
                В этом массиве содержатся элементы m[-2], m[-1], m[n], m[n+1].
                Они будут использованы в дальнейшем, но могут быть вычислены в m.            
            """
            _m = np.array(np.zeros(4))
            tl = tr = np.array(np.zeros(n + 1))

            # для широты переодические ГУ
            if direction == 'latitude':
                _m[0] = m[-2]
                _m[1] = m[-1]
                _m[2] = 2 * m[0]
                _m[3] = 3 * m[1]

            # для долготы естественные ГУ
            elif direction == 'longitude':
                _m[0] = 3 * m[0] - 2 * m[1]
                _m[1] = 2 * m[0] - 2 * m[1]
                _m[2] = 2 * m[-1] - 2 * m[-2]
                _m[3] = 3 * m[-1] - 2 * m[-2]

            m1 = np.concatenate((_m[:2], m, _m[2:]))
			
            dm = np.abs(np.diff(m1))
            f1 = dm[2:(n + 3)]
            f2 = dm[0:(n + 1)]
            f12 = f1 + f2

            ids = np.nonzero(f12 > 1e-9 * np.max(f12))[0]
            b = m1[1:(n + 2)]

            b[ids] = (f1[ids] * m1[ids + 1] + f2[ids] * m1[ids + 2]) / f12[ids]
            c = (3.0 * m - 2.0 * b[0:n] - b[1:(n + 1)]) / step
            d = (b[0:n] + b[1:(n + 1)] - 2.0 * m) / step ** 2

            res = np.column_stack((a, b[:-1], c * 2, d * 6))

            return res

        def get_value(z: np.matrix, bx: np.matrix, by: np.matrix, bxy: np.matrix, point: tuple) -> float:
            f1 = lambda x: (1 - x) * (1 - x) * (1 + 2 * x)
            f2 = lambda x: x * x * (3 - 2 * x)
            f3 = lambda x: x * (1 - x) * (1 - x)
            f4 = lambda x: -x * x * (1 - x)

            t, u = point
            a = np.matrix([f1(t), f2(t), f3(t), f4(t)])
            b = np.matrix([f1(u), f2(u), f3(u), f4(u)]).T
            M = np.vstack((np.hstack((z, bx)), np.hstack((by, bxy))))

            return (a @ M @ b)

        m, n = source_points.shape
        x_dist, y_dist = dist_calc(source_points.shape[0], source_points.shape[1])
        x_dist, y_dist = map(lambda x: x*2, (x_dist, y_dist))
        res = np.empty((m, n), dtype=object)

        for k in range(4):
            if k == 0:
                work_matr = copy.deepcopy(source_points[::2, ::2])
                work_matr = np.vstack((work_matr[1] - work_matr[0], work_matr, work_matr[-1] - work_matr[-2]))
                work_matr = work_matr[::-1]
                tmp_x_dist = copy.deepcopy(x_dist[::2])
                tmp_x_dist = np.hstack((tmp_x_dist[0], tmp_x_dist, tmp_x_dist[-1]))
                di = dj = 0
                sp = copy.deepcopy(source_points[::2, ::2])

            elif k == 1:
                work_matr = copy.deepcopy(source_points[1::2, ::2])
                work_matr = np.vstack((work_matr[1] - work_matr[0], work_matr, work_matr[-1] - work_matr[-2]))
                work_matr = work_matr[::-1]
                tmp_x_dist = copy.deepcopy(x_dist[1::2])
                tmp_x_dist = np.hstack((tmp_x_dist[0], tmp_x_dist, tmp_x_dist[-1]))
                di = 1
                dj = 0
                sp = copy.deepcopy(source_points[1::2, ::2])

            elif k == 2:
                work_matr = copy.deepcopy(source_points[::2, 1::2])
                work_matr = np.vstack((work_matr[1] - work_matr[0], work_matr, work_matr[-1] - work_matr[-2]))
                work_matr = work_matr[::-1]
                tmp_x_dist = copy.deepcopy(x_dist[::2])
                tmp_x_dist = np.hstack((tmp_x_dist[0], tmp_x_dist, tmp_x_dist[-1]))
                di = 0
                dj = 1
                sp = copy.deepcopy(source_points[::2, 1::2])

            elif k == 3:
                work_matr = copy.deepcopy(source_points[1::2, 1::2])
                work_matr = np.vstack((work_matr[1] - work_matr[0], work_matr, work_matr[-1] - work_matr[-2]))
                work_matr = work_matr[::-1]
                tmp_x_dist = copy.deepcopy(x_dist[1::2])
                tmp_x_dist = np.hstack((tmp_x_dist[0], tmp_x_dist, tmp_x_dist[-1]))
                di = dj = 1
                sp = copy.deepcopy(source_points[1::2, 1::2])


            mm, nn = sp.shape
            for i in range(mm + 2):
                if i > 0:
                    bx = np.vstack((bx, akima_spline(work_matr[i].A1, tmp_x_dist[i], 'latitude')[:, 1]))
                else:
                    bx = akima_spline(work_matr[i].A1, tmp_x_dist[i], 'latitude')[:, 1]
            bx = np.column_stack((bx, bx[:, 0]))

            for j in range(nn):
                if j > 0:
                    by = np.column_stack((by, akima_spline(work_matr[:, j].A1, y_dist, 'longitude')[:, 1]))

                    bxy = np.column_stack((bxy, akima_spline(bx[:, j], y_dist, 'longitude')[:, 1]))
                else:
                    by = akima_spline(work_matr[:, j].A1, y_dist, 'longitude')[:, 1]

                    bxy = akima_spline(bx[:, j], y_dist, 'longitude')[:, 1]

            bx, by, bxy = map(lambda x: np.matrix(x), (bx, by, bxy))
            bx, by, bxy = map(lambda x: x[1: -1], (bx, by, bxy))
            bx, by, bxy = map(lambda x: x[::-1], (bx, by, bxy))
            by, bxy = map(lambda x: np.vstack((x, x[0])), (by, bxy))

            mm, nn = sp.shape

            # перебор строк
            for i in range(0, mm - 1):
                # перебор столбцов
                for j in range(0, nn - 1):
                    res[2*i + di, 2*j + dj + 1] = add_value(res[2*i + di, 2*j + dj + 1], get_value(sp[i:(i + 2), j:(j + 2)],
                                                                                           bx[i:(i + 2), j:(j + 2)],
                                                                                           by[i:(i + 2), j:(j + 2)],
                                                                                           bxy[i:(i + 2), j:(j + 2)],
                                                                                           (0, 0.5)))
                    res[2*i + di + 1, 2*j + dj] = add_value(res[2*i + di + 1, 2*j + dj], get_value(sp[i:(i + 2), j:(j + 2)],
                                                                                           bx[i:(i + 2), j:(j + 2)],
                                                                                           by[i:(i + 2), j:(j + 2)],
                                                                                           bxy[i:(i + 2), j:(j + 2)],
                                                                                           (0.5, 0)))
                    res[2*i + di + 1, 2*j + dj + 1] = add_value(res[2*i + di + 1, 2*j + dj + 1], get_value(sp[i:(i + 2), j:(j + 2)],
                                                                                                   bx[i:(i + 2), j:(j + 2)],
                                                                                                   by[i:(i + 2), j:(j + 2)],
                                                                                                   bxy[i:(i + 2), j:(j + 2)],
                                                                                                   (0.5, 0.5)))
                    res[2*i + di + 1, 2*j + dj + 2] = add_value(res[2*i + di + 1, 2*j + dj + 2], get_value(sp[i:(i + 2), j:(j + 2)],
                                                                                                   bx[i:(i + 2), j:(j + 2)],
                                                                                                   by[i:(i + 2), j:(j + 2)],
                                                                                                   bxy[i:(i + 2), j:(j + 2)],
                                                                                                   (0.5, 1)))
                    res[2*i + di + 2, 2*j + dj + 1] = add_value(res[2*i + di + 2, 2*j + dj + 1], get_value(sp[i:(i + 2), j:(j + 2)],
                                                                                                   bx[i:(i + 2), j:(j + 2)],
                                                                                                   by[i:(i + 2), j:(j + 2)],
                                                                                                   bxy[i:(i + 2), j:(j + 2)],
                                                                                                   (1, 0.5)))
            del(bx)
            del(by)
            del(bxy)

        return res
		
    if kind == 'bicubic':
        return bicub_validation()
    elif kind == 'bilinear':
        return bilin_validation()
    elif kind == 'cubic_splines':
        return cub_splines_validation()
    elif kind == 'akima_splines':
        return akima_splines_validation()
    else:
        raise ValueError('Incorrect function arguments')
# ======================================================================================================================


# ======================================================================================================================
"""
    source_points : np.matrix
        Входная матрица температур, которую хотим проинтерполировать.
    delta : int
        Радиус дельта-окрестности (в метрах)
    kind : { 'bilinear', 'bicubic', 'cubic_splines', 'akima_splines', 'cubic_B_splines', 'rbf', 'smooth_splines', 'bezier' }, optional    
        Тип интерполяции, кросс-валидацию которой хотим провести.
        Default is 'bicubic'.
    
    Вычисляет матрицу значений в окрестности радиуса delta
"""
def derivative_limit(source_points: np.matrix, delta=1, kind='bicubic') -> np.matrix:
    x_dist, y_dist = dist_calc(source_points.shape[0], source_points.shape[1])

    def bilin_derivative_limit() -> np.matrix:
        def bilin_interp(source_matrix: np.matrix, point: tuple) -> float:
            x, y = point

            return np.matrix([(1 - x), x]) @ source_matrix @ np.matrix([[1 - y], [y]])

        def delta_circle(matr:np.matrix, x: int, y: int, dy: int, dx: int) -> np.array:
            dhor = delta / x_dist[x]
            dver = delta / y_dist
            res = np.empty(8)

            k = math.sqrt(2) / 2
            # обходим с верхнего
            res[0] = bilin_interp(matr, (dy, dx - dver))
            res[1] = bilin_interp(matr, (dy + dhor * k, dx - dver * k))
            res[2] = bilin_interp(matr, (dy + dhor, dx))
            res[3] = bilin_interp(matr, (dy + dhor * k, dx + dver * k))
            res[4] = bilin_interp(matr, (dy, dx + dver))
            res[5] = bilin_interp(matr, (dy - dhor * k, dx + dver * k))
            res[6] = bilin_interp(matr, (dy - dhor * k, dx))
            res[7] = bilin_interp(matr, (dy - dhor * k,dx - dver * k))

            return res.round(6)

        m, n = source_points.shape
        res = np.empty((m, n), dtype=object)

        # перебор строк
        for i in range(m - 1):
            # перебор столбцов
            for j in range(n - 1):
                m_m = source_points[i:(i+2), j:(j+2)]

                res[i, j] = delta_circle(m_m, i, j, 0, 0)
                res[i, j + 1] = delta_circle(m_m, i, j + 1, 0, 1)
                res[i + 1, j + 1] = delta_circle(m_m, i + 1, j + 1, 1, 1)
                res[i + 1, j] = delta_circle(m_m, i + 1, j, 1, 0)

        return res

    def bicub_derivative_limit() -> np.matrix:
        def bicub_interp(source_matrix: np.matrix, point: tuple) -> float:
            x, y = point

            def cubic_interpolation(p: np.array, m_x: float) -> float:
                res = p[1] + (-0.5 * p[0] + 0.5 * p[2]) * m_x + (
                        p[0] - 2.5 * p[1] + 2.0 * p[2] - 0.5 * p[3]) * m_x * m_x \
                      + (-0.5 * p[0] + 1.5 * p[1] - 1.5 * p[2] + 0.5 * p[3]) * m_x * m_x * m_x
                return res

            tmp = list()
            tmp.append(cubic_interpolation(source_matrix[0].A1, x))
            tmp.append(cubic_interpolation(source_matrix[1].A1, x))
            tmp.append(cubic_interpolation(source_matrix[2].A1, x))
            tmp.append(cubic_interpolation(source_matrix[3].A1, x))
            tmp = np.array(tmp)

            return cubic_interpolation(tmp, y)

        def delta_circle(matr:np.matrix, x: int, y: int, dy: int, dx: int) -> np.array:
            dhor = delta / x_dist[x]
            dver = delta / y_dist
            res = np.empty(8)

            k = math.sqrt(2) / 2
            # обходим с верхнего
            res[0] = bicub_interp(matr, (dx - dver, dy))
            res[1] = bicub_interp(matr, (dx - dver * k, dy + dhor * k))
            res[2] = bicub_interp(matr, (dx, dy + dhor))
            res[3] = bicub_interp(matr, (dx + dver * k, dy + dhor * k))
            res[4] = bicub_interp(matr, (dx + dver, dy))
            res[5] = bicub_interp(matr, (dx + dver * k, dy - dhor * k))
            res[6] = bicub_interp(matr, (dx, dy - dhor * k))
            res[7] = bicub_interp(matr, (dx - dver * k, dy - dhor * k))

            return res.round(6)

        matr = np.vstack((2 * source_points[0, :] - source_points[1, :], source_points,
                          2 * source_points[-1, :] - source_points[-2, :]))
        matr = np.hstack((matr[:, -1], matr, matr[:, 0]))

        m, n = source_points.shape
        res = np.empty((m, n), dtype=object)

        # перебор строк
        for i in range(m - 1):
            # перебор столбцов
            for j in range(n - 1):
                m_m = matr[i:(i+4), j:(j+4)]

                res[i, j] = delta_circle(m_m, i, j, 0, 0)
                res[i, j + 1] = delta_circle(m_m, i, j + 1, 0, 1)
                res[i + 1, j + 1] = delta_circle(m_m, i + 1, j + 1, 1, 1)
                res[i + 1, j] = delta_circle(m_m, i + 1, j, 1, 0)

        return res

    def cub_splines_derivative_limit() -> np.matrix:
        def cubic_spline(points: np.array, step: int, direction: str) -> np.matrix:
            n = points.size - 1
            res = np.matrix(np.zeros((n, 4)))

            a = copy.deepcopy(points[1:])
            b = np.array(np.zeros(n))
            c = np.array(np.zeros(n))
            d = np.array(np.zeros(n))

            f = lambda x: (points[(n + x + 1) % n] - points[(n + x) % n]) / step

            # помнить про то, что индексация матрицы начинается с 0, а индексы коэффициентов с 1
            left = np.matrix(np.zeros((n - 1, n)))
            right = 6 / step * np.array(np.ones(n - 1))
            for i in range(2, n + 1):
                if i < n:
                    left[i - 2, i - 1] = 1
                left[i - 2, i - 2] = 4
                if i > 2:
                    left[i - 2, i - 3] = 1
                right[i - 2] *= f(i - 2) - f(i - 1)
            right = right.reshape((n - 1, 1))
            c = lstsq(left, right)[0]

            # для широты переодические ГУ
            if direction == 'latitude':
                for i in range(2, n + 1):
                    b[i - 1] = f(i - 1) + step * (c[i - 1] / 3 + c[i - 2] / 6)
                    d[i - 1] = (c[i - 1] - c[i - 2]) / step
                b[0] = f(0) + step * (c[0] / 3 + c[-1] / 6)
                # b[0] = b[-1] + step * (c[-1] + step*d[-1]/2)
                d[0] = d[-1]

                res = np.column_stack((a, b, c, d))

            # для долготы естественные ГУ
            elif direction == 'longitude':
                for i in range(2, n + 1):
                    b[i - 1] = f(i - 1) + step * (c[i - 1] / 3 + c[i - 2] / 6)
                    d[i - 1] = (c[i - 1] - c[i - 2]) / step
                b[0] = f(0) + step * (c[0] / 3)
                d[0] = (c[0]) / step

                res = np.column_stack((a, b, c, d))

            return res
        
        def delta_circle(x: int, y: int, dx: int, dy: int) -> np.array:
            dhor = delta / x_dist[x]
            dver = delta / y_dist
            res = np.empty(8)

            k = math.sqrt(2) / 2
            xx, yy = map(lambda k, dk: k - dk, (x, y), (dx, dy))
            msp = source_points[xx:(xx + 2), yy:(yy + 2)]
            mbx = bx[xx:(xx + 2), yy:(yy + 2)]
            mby = by[xx:(xx + 2), yy:(yy + 2)]
            mbxy = bxy[xx:(xx + 2), yy:(yy + 2)]

            # обходим с верхнего
            res[0] = get_value(msp, mbx, mby, mbxy, (dx - dver, dy))
            res[1] = get_value(msp, mbx, mby, mbxy, (dx - dver * k, dy + dhor * k))
            res[2] = get_value(msp, mbx, mby, mbxy, (dx, dy + dhor))
            res[3] = get_value(msp, mbx, mby, mbxy, (dx + dver * k, dy + dhor * k))
            res[4] = get_value(msp, mbx, mby, mbxy, (dx + dver, dy))
            res[5] = get_value(msp, mbx, mby, mbxy, (dx + dver * k, dy - dhor * k))
            res[6] = get_value(msp, mbx, mby, mbxy, (dx, dy - dhor * k))
            res[7] = get_value(msp, mbx, mby, mbxy, (dx - dver * k, dy - dhor * k))

            return res.round(6)

        def get_value(z: np.matrix, bx: np.matrix, by: np.matrix, bxy: np.matrix, point: tuple) -> float:
            f1 = lambda x: (1 - x) * (1 - x) * (1 + 2 * x)
            f2 = lambda x: x * x * (3 - 2 * x)
            f3 = lambda x: x * (1 - x) * (1 - x)
            f4 = lambda x: -x * x * (1 - x)

            t, u = point
            a = np.matrix([f1(t), f2(t), f3(t), f4(t)])
            b = np.matrix([f1(u), f2(u), f3(u), f4(u)]).T
            M = np.vstack((np.hstack((z, bx)), np.hstack((by, bxy))))

            return (a @ M @ b)

        m, n = source_points.shape
        tmp_x_dist = np.hstack((x_dist[0], x_dist, x_dist[-1]))

        work_matr = copy.deepcopy(source_points)
        work_matr = np.vstack((work_matr[1] - work_matr[0], work_matr, work_matr[-1] - work_matr[-2]))
        work_matr = work_matr[::-1]

        for i in range(m + 2):
            if i > 0:
                bx = np.vstack((bx, cubic_spline(work_matr[i].A1, tmp_x_dist[i], 'latitude')[:, 1]))
            else:
                bx = cubic_spline(work_matr[i].A1, tmp_x_dist[i], 'latitude')[:, 1]
        bx = np.column_stack((bx, bx[:, 0]))

        for j in range(n):
            if j > 0:
                by = np.column_stack((by, cubic_spline(work_matr[:, j].A1, y_dist, 'longitude')[:, 1]))

                bxy = np.column_stack((bxy, cubic_spline(bx[:, j], y_dist, 'longitude')[:, 1]))
            else:
                by = cubic_spline(work_matr[:, j].A1, y_dist, 'longitude')[:, 1]

                bxy = cubic_spline(bx[:, j], y_dist, 'longitude')[:, 1]

        bx, by, bxy = map(lambda x: np.matrix(x), (bx, by, bxy))
        bx, by, bxy = map(lambda x: x[1: -1], (bx, by, bxy))
        bx, by, bxy = map(lambda x: x[::-1], (bx, by, bxy))
        by, bxy = map(lambda x: np.vstack((x, x[0])), (by, bxy))

        res = np.empty((m, n), dtype=object)
    
        # перебор строк
        for i in range(m - 1):
            # перебор столбцов
            for j in range(n - 1):
                res[i, j] = delta_circle(i, j, 0, 0)
                res[i, j + 1] = delta_circle(i, j + 1, 0, 1)
                res[i + 1, j + 1] = delta_circle(i + 1, j + 1, 1, 1)
                res[i + 1, j] = delta_circle(i + 1, j, 1, 0)
        
        return res

    def akima_splines_derivative_limit() -> np.matrix:
        def akima_spline(points: np.array, step: int, direction: str) -> np.matrix:
            n = points.size - 1
            res = np.matrix(np.zeros((n, 4)))

            a = copy.deepcopy(points[1:])
            b = np.array(np.zeros(n))
            c = np.array(np.zeros(n))
            d = np.array(np.zeros(n))

            m = np.diff(points) / step
            """
                В этом массиве содержатся элементы m[-2], m[-1], m[n], m[n+1].
                Они будут использованы в дальнейшем, но могут быть вычислены в m.            
            """
            _m = np.array(np.zeros(4))
            tl = tr = np.array(np.zeros(n + 1))

            # для широты переодические ГУ
            if direction == 'latitude':
                _m[0] = m[-2]
                _m[1] = m[-1]
                _m[2] = 2 * m[0]
                _m[3] = 3 * m[1]

            # для долготы естественные ГУ
            elif direction == 'longitude':
                _m[0] = 3 * m[0] - 2 * m[1]
                _m[1] = 2 * m[0] - 2 * m[1]
                _m[2] = 2 * m[-1] - 2 * m[-2]
                _m[3] = 3 * m[-1] - 2 * m[-2]

            m1 = np.concatenate((_m[:2], m, _m[2:]))
			
            dm = np.abs(np.diff(m1))
            f1 = dm[2:(n + 3)]
            f2 = dm[0:(n + 1)]
            f12 = f1 + f2

            ids = np.nonzero(f12 > 1e-9 * np.max(f12))[0]
            b = m1[1:(n + 2)]

            b[ids] = (f1[ids] * m1[ids + 1] + f2[ids] * m1[ids + 2]) / f12[ids]
            c = (3.0 * m - 2.0 * b[0:n] - b[1:(n + 1)]) / step
            d = (b[0:n] + b[1:(n + 1)] - 2.0 * m) / step ** 2

            res = np.column_stack((a, b[:-1], c * 2, d * 6))

            return res
        
        def delta_circle(x: int, y: int, dx: int, dy: int) -> np.array:
            dhor = delta / x_dist[x]
            dver = delta / y_dist
            res = np.empty(8)

            k = math.sqrt(2) / 2
            xx, yy = map(lambda k, dk: k - dk, (x, y), (dx, dy))
            msp = source_points[xx:(xx + 2), yy:(yy + 2)]
            mbx = bx[xx:(xx + 2), yy:(yy + 2)]
            mby = by[xx:(xx + 2), yy:(yy + 2)]
            mbxy = bxy[xx:(xx + 2), yy:(yy + 2)]

            # обходим с верхнего
            res[0] = get_value(msp, mbx, mby, mbxy, (dx - dver, dy))
            res[1] = get_value(msp, mbx, mby, mbxy, (dx - dver * k, dy + dhor * k))
            res[2] = get_value(msp, mbx, mby, mbxy, (dx, dy + dhor))
            res[3] = get_value(msp, mbx, mby, mbxy, (dx + dver * k, dy + dhor * k))
            res[4] = get_value(msp, mbx, mby, mbxy, (dx + dver, dy))
            res[5] = get_value(msp, mbx, mby, mbxy, (dx + dver * k, dy - dhor * k))
            res[6] = get_value(msp, mbx, mby, mbxy, (dx, dy - dhor * k))
            res[7] = get_value(msp, mbx, mby, mbxy, (dx - dver * k, dy - dhor * k))

            return res.round(6)

        def get_value(z: np.matrix, bx: np.matrix, by: np.matrix, bxy: np.matrix, point: tuple) -> float:
            f1 = lambda x: (1 - x) * (1 - x) * (1 + 2 * x)
            f2 = lambda x: x * x * (3 - 2 * x)
            f3 = lambda x: x * (1 - x) * (1 - x)
            f4 = lambda x: -x * x * (1 - x)

            t, u = point
            a = np.matrix([f1(t), f2(t), f3(t), f4(t)])
            b = np.matrix([f1(u), f2(u), f3(u), f4(u)]).T
            M = np.vstack((np.hstack((z, bx)), np.hstack((by, bxy))))

            return (a @ M @ b)

        m, n = source_points.shape
        tmp_x_dist = np.hstack((x_dist[0], x_dist, x_dist[-1]))

        work_matr = copy.deepcopy(source_points)
        work_matr = np.vstack((work_matr[1] - work_matr[0], work_matr, work_matr[-1] - work_matr[-2]))
        work_matr = work_matr[::-1]

        for i in range(m + 2):
            if i > 0:
                bx = np.vstack((bx, akima_spline(work_matr[i].A1, tmp_x_dist[i], 'latitude')[:, 1]))
            else:
                bx = akima_spline(work_matr[i].A1, tmp_x_dist[i], 'latitude')[:, 1]
        bx = np.column_stack((bx, bx[:, 0]))

        for j in range(n):
            if j > 0:
                by = np.column_stack((by, akima_spline(work_matr[:, j].A1, y_dist, 'longitude')[:, 1]))

                bxy = np.column_stack((bxy, akima_spline(bx[:, j], y_dist, 'longitude')[:, 1]))
            else:
                by = akima_spline(work_matr[:, j].A1, y_dist, 'longitude')[:, 1]

                bxy = akima_spline(bx[:, j], y_dist, 'longitude')[:, 1]

        bx, by, bxy = map(lambda x: np.matrix(x), (bx, by, bxy))
        bx, by, bxy = map(lambda x: x[1: -1], (bx, by, bxy))
        bx, by, bxy = map(lambda x: x[::-1], (bx, by, bxy))
        by, bxy = map(lambda x: np.vstack((x, x[0])), (by, bxy))

        res = np.empty((m, n), dtype=object)
    
        # перебор строк
        for i in range(m - 1):
            # перебор столбцов
            for j in range(n - 1):
                res[i, j] = delta_circle(i, j, 0, 0)
                res[i, j + 1] = delta_circle(i, j + 1, 0, 1)
                res[i + 1, j + 1] = delta_circle(i + 1, j + 1, 1, 1)
                res[i + 1, j] = delta_circle(i + 1, j, 1, 0)
        
        return res
		
    def cubic_B_splines_derivative_limit() -> np.matrix:
        def delta_circle(x: int, y: int) -> np.array:
            dhor = delta / x_dist[x]
            dver = delta / y_dist
            res = np.empty(8)

            k = math.sqrt(2) / 2
            # обходим с верхнего
            res[0] = test(y, x - dver)
            res[1] = test(y + dhor * k, x - dver * k)
            res[2] = test(y + dhor, x)
            res[3] = test(y + dhor * k, x + dver * k)
            res[4] = test(y, x + dver)
            res[5] = test(y - dhor * k, x + dver * k)
            res[6] = test(y - dhor * k, x)
            res[7] = test(y - dhor * k, x - dver * k)

            return res.round(6)

        m, n = source_points.shape
        res = np.empty((m, n), dtype=object)
        test = interpolate.interp2d(np.arange(n), np.arange(m), source_points, kind='cubic')

        # перебор строк
        for i in range(m - 1):
            # перебор столбцов
            for j in range(n - 1):
                res[i, j] = delta_circle(i, j)
                res[i, j + 1] = delta_circle(i, j + 1)
                res[i + 1, j + 1] = delta_circle(i + 1, j + 1)
                res[i + 1, j] = delta_circle(i + 1, j)

        return res

    def rbf_derivative_limit() -> np.matrix:
        def delta_circle(x: int, y: int) -> np.array:
            dhor = delta / x_dist[x]
            dver = delta / y_dist
            res = np.empty(8)

            k = math.sqrt(2) / 2
            # обходим с верхнего
            res[0] = rbfi(y, x - dver)
            res[1] = rbfi(y + dhor * k, x - dver * k)
            res[2] = rbfi(y + dhor, x)
            res[3] = rbfi(y + dhor * k, x + dver * k)
            res[4] = rbfi(y, x + dver)
            res[5] = rbfi(y - dhor * k, x + dver * k)
            res[6] = rbfi(y - dhor * k, x)
            res[7] = rbfi(y - dhor * k, x - dver * k)

            return res.round(6)

        m, n = source_points.shape
        res = np.empty((m, n), dtype=object)
        x, y = np.meshgrid(np.arange(n), np.arange(m))
        rbfi = interpolate.Rbf(x, y, source_points.A1, function='multiquadric')

        # перебор строк
        for i in range(m - 1):
            # перебор столбцов
            for j in range(n - 1):
                res[i, j] = delta_circle(i, j)
                res[i, j + 1] = delta_circle(i, j + 1)
                res[i + 1, j + 1] = delta_circle(i + 1, j + 1)
                res[i + 1, j] = delta_circle(i + 1, j)

        return res

    def smooth_splines_derivative_limit() -> np.matrix:
        def delta_circle(x: int, y: int) -> np.array:
            dhor = delta / x_dist[x]
            dver = delta / y_dist
            res = np.empty(8)

            k = math.sqrt(2) / 2
            # обходим с верхнего
            res[0] = sbsp(y, x - dver)
            res[1] = sbsp(y + dhor * k, x - dver * k)
            res[2] = sbsp(y + dhor, x)
            res[3] = sbsp(y + dhor * k, x + dver * k)
            res[4] = sbsp(y, x + dver)
            res[5] = sbsp(y - dhor * k, x + dver * k)
            res[6] = sbsp(y - dhor * k, x)
            res[7] = sbsp(y - dhor * k, x - dver * k)

            return res.round(6)

        m, n = source_points.shape
        res = np.empty((m, n), dtype=object)
        x, y = np.meshgrid(np.arange(n), np.arange(m))
        sbsp = interpolate.SmoothBivariateSpline(x.flat, y.flat, source_points.A1)

        # перебор строк
        for i in range(m - 1):
            # перебор столбцов
            for j in range(n - 1):
                res[i, j] = delta_circle(i, j)
                res[i, j + 1] = delta_circle(i, j + 1)
                res[i + 1, j + 1] = delta_circle(i + 1, j + 1)
                res[i + 1, j] = delta_circle(i + 1, j)

        return res

    def bezier_derivative_limit() -> np.matrix:
        dim = 3
        def prepare_data(matr=source_points, add_rows=2, add_cols=2):
            if add_rows == 0:
                if add_cols == 0:
                    return matr
                elif add_cols == 1:
                    return np.hstack((matr, matr[:, 0]))
                elif add_cols == 2:
                    return np.hstack((matr, matr[:, 0:2]))

            # экстраполируем сетку на одну строку вверх
            elif add_rows == 1:
                res = np.vstack((matr, 2 * matr[-1, :] - matr[-2, :]))
                if add_cols == 0:
                    return res
                elif add_cols == 1:
                    return np.hstack((res, res[:, 0]))
                elif add_cols == 2:
                    return np.hstack((res, res[:, 0:2]))

            # экстраполируем сетку на одну строку вверх и вниз
            elif add_rows == 2:
                res = np.vstack((2 * matr[0, :] - matr[1, :], matr, 2 * matr[-1, :] - matr[-2, :]))
                if add_cols == 0:
                    return res
                elif add_cols == 1:
                    return np.hstack((res, res[:, 0]))
                elif add_cols == 2:
                    return np.hstack((res, res[:, 0:2]))

        def bezier_curve(points: np.array, t: float) -> float:
            b0 = (1 - t) * (1 - t) * (1 - t)
            b1 = 3 * t * (1 - t) * (1 - t)
            b2 = 3 * t * t * (1 - t)
            b3 = t * t * t

            return points[0] * b0 + points[1] * b1 + points[2] * b2 + points[3] * b3

        def bezier_surface(points: np.array, additional_point: tuple) -> float:
            u, v = additional_point

            curve = np.array(np.zeros(4))
            for i in range(dim + 1):
                curve_u = np.array(np.zeros(4))
                curve_u[0] = points[i * 4]
                curve_u[1] = points[i * 4 + 1]
                curve_u[2] = points[i * 4 + 2]
                curve_u[3] = points[i * 4 + 3]
                curve[i] = bezier_curve(curve_u, u)

            return bezier_curve(curve, v)

        def delta_circle(matr:np.matrix, x: int, y: int, dy: float, dx: float) -> np.array:
            dhor = delta / tmp_x_dist[x]
            dver = delta / y_dist
            res = np.empty(8)

            k = math.sqrt(2) / 6
            # обходим с верхнего
            res[0] = bezier_surface(matr.A1, (dx - dver, dy))
            res[1] = bezier_surface(matr.A1, (dx - dver * k, dy + dhor * k))
            res[2] = bezier_surface(matr.A1, (dx, dy + dhor))
            res[3] = bezier_surface(matr.A1, (dx + dver * k, dy + dhor * k))
            res[4] = bezier_surface(matr.A1, (dx + dver, dy))
            res[5] = bezier_surface(matr.A1, (dx + dver * k, dy - dhor * k))
            res[6] = bezier_surface(matr.A1, (dx, dy - dhor * k))
            res[7] = bezier_surface(matr.A1, (dx - dver * k, dy - dhor * k))

            return res.round(6)

        m, n = source_points.shape
        mm, nn = map(lambda xx: 3 - ((xx - 4) % 3), (m, n))
        m_matr = prepare_data(source_points, add_rows=mm, add_cols=nn)
        if mm == 0:
            tmp_x_dist = x_dist
        elif mm == 1:
            tmp_x_dist = np.hstack((x_dist, x_dist[-1]))
        elif mm == 2:
            tmp_x_dist = np.hstack((x_dist[0], x_dist, x_dist[-1]))
        res = np.empty((m + mm, n + nn), dtype=object)

        # перебор строк
        for i in range(0, m, 3):
            # перебор столбцов
            for j in range(0, n, 3):
                m_m = m_matr[i:(i+4), j:(j+4)]

                res[i, j] = delta_circle(m_m, i, j, 0, 0)
                res[i, j + 1] = delta_circle(m_m, i, j + 1, 0, 1/3)
                res[i + 1, j + 1] = delta_circle(m_m, i + 1, j + 1, 1/3, 1/3)
                res[i + 1, j] = delta_circle(m_m, i + 1, j, 1/3, 0)

                res[i, j + 2] = delta_circle(m_m, i, j + 2, 0, 2/3)
                res[i, j + 3] = delta_circle(m_m, i, j + 3, 0, 1)
                res[i + 1, j + 3] = delta_circle(m_m, i + 1, j + 3, 1/3, 1)
                res[i + 1, j + 2] = delta_circle(m_m, i + 1, j + 2, 1/3, 2/3)

                res[i + 2, j] = delta_circle(m_m, i + 2, j, 2/3, 0)
                res[i + 2, j + 1] = delta_circle(m_m, i + 2, j + 1, 2/3, 1/3)
                res[i + 3, j + 1] = delta_circle(m_m, i + 3, j + 1, 1, 1/3)
                res[i + 3, j] = delta_circle(m_m, i + 3, j, 1, 0)

                res[i + 2, j + 2] = delta_circle(m_m, i + 2, j + 2, 2/3, 2/3)
                res[i + 2, j + 3] = delta_circle(m_m, i + 2, j + 3, 2/3, 1)
                res[i + 3, j + 3] = delta_circle(m_m, i + 3, j + 3, 1, 1)
                res[i + 3, j + 2] = delta_circle(m_m, i + 3, j + 2, 1, 2/3)

        if mm == 0:
            if nn == 0:
                return res[:, :]
            elif nn == 1:
                return res[:, :-1]
            elif nn == 2:
                return res[:, :-2]

        elif mm == 1:
            if nn == 0:
                return res[1:, :]
            elif nn == 1:
                return res[1:, :-1]
            elif nn == 2:
                return res[1:, :-2]

        elif mm == 2:
            if nn == 0:
                return res[1:-1, :]
            elif nn == 1:
                return res[1:-1, :-1]
            elif nn == 2:
                return res[1:-1, :-2]

    if kind == 'bilinear':
        return bilin_derivative_limit()
    elif kind == 'bicubic':
        return bicub_derivative_limit()
    elif kind == 'cubic_splines':
        return cub_splines_derivative_limit()
    elif kind == 'akima_splines':
        return akima_splines_derivative_limit()
    elif kind == 'cubic_B_splines':
        return cubic_B_splines_derivative_limit()
    elif kind == 'rbf':
        return rbf_derivative_limit()
    elif kind == 'smooth_splines':
        return smooth_splines_derivative_limit()
    elif kind == 'bezier':
        return bezier_derivative_limit()
    else:
        raise ValueError('Incorrect function arguments')
# ======================================================================================================================


# ======================================================================================================================
"""
    interp : np.matrix
        Проинтерполированная матрица.
    orig : np.matrix
        Оригинальная матрица.

    Вычисляем матрицу отличий значений проинтерполированной от оригинальной в исходных точках
"""
def calc_difference_matrix(interp: np.matrix, orig: np.matrix) -> np.matrix:
    m, n = orig.shape
    mm, nn = interp.shape
    add_count = round((mm - m) / (m - 1))

    return interp[::(add_count + 1), ::(add_count + 1)] - orig
# ======================================================================================================================


# ======================================================================================================================
"""
    interp : np.matrix
        Проинтерполированная матрица (результат кросс-валидациии или матрица отлиций для аппроксимаций).
    orig : np.matrix
        Оригинальная матрица.

    Вычисляем среднеквадратичную ошибку для каждого элемента матрицы
"""
def calc_mse(interp: np.matrix, orig: np.matrix) -> np.matrix:
    m, n = orig.shape
    res = np.zeros(orig.shape)
 
    # перебор по строкам
    for i in range(m):
        # перебор по столбцам
        for j in range(n):
            if type(interp[i, j]) is np.float64:
                res[i, j] = round(math.sqrt((interp[i, j] - orig[i, j]) ** 2), 6)
            elif interp[i, j] is not None:
                res[i, j] = round(math.sqrt(((interp[i, j] - orig[i, j]) ** 2).sum() / len(interp[i, j])), 6)
            else:
                res[i, j] = 0

    return res
# ======================================================================================================================


# ======================================================================================================================
"""
    interp : np.matrix
        Проинтерполированная матрица (результат кросс-валидациии или матрица отлиций для аппроксимаций).
    orig : np.matrix
        Оригинальная матрица.

    Вычисляем среднее отклонение для каждого элемента матрицы
"""
def calc_md(interp: np.matrix, orig: np.matrix) -> np.matrix:
    m, n = orig.shape
    res = np.zeros(orig.shape)

    # перебор по строкам
    for i in range(m):
        # перебор по столбцам
        for j in range(n):
            if type(interp[i, j]) is np.float64:
                res[i, j] = round((interp[i, j] - orig[i, j]), 6)
            elif interp[i, j] is not None:
                res[i, j] = round(((interp[i, j] - orig[i, j]).sum() / len(interp[i, j])), 6)
            else:
                res[i, j] = 0

    return res
# ======================================================================================================================


# ======================================================================================================================
"""
    data : np.matrix
        Входная матрица температур, тепловую карту которой хотим отрисовать.
    discrete : int
        Степень дискретизации цветов.
        Default is 3.
    colormap : str
        Задаёт карту цветов из списка matplotlib.
        Default is 'coolwarm'.
        
    Простая рисовалка тепловой карты
"""
def paint_coolwarm_map(data: np.matrix, discrete=40, colormap='coolwarm'):
    m, n = data.shape

    fig, ax = plt.subplots()
    heatmap = ax.pcolor(data.A, cmap=plt.cm.get_cmap(colormap, discrete))
    ax.set_yticks(np.arange(m) + 0.5, minor=False)
    ax.set_xticks(np.arange(n) + 0.5, minor=False)

    divider = make_axes_locatable(plt.gca())
    cax = divider.append_axes("right", "5%", pad="3%")
    fig.colorbar(heatmap, cax=cax)

    fig.set_size_inches(19.2, 10.8, forward=True)

    ax.set_xticklabels('x', minor=False)
    ax.set_yticklabels('y', minor=False)
    fig.tight_layout()
    plt.show()

    return
# ======================================================================================================================


# ======================================================================================================================
"""
    data : np.matrix
        Входная матрица температур, для которой строим поверхность.
    discrete : int
        Степень дискретизации цветов.
        Default is 3.
    colormap : str
        Задаёт карту цветов из списка matplotlib.
        Default is 'coolwarm'.
        
    Рисовалка трёхмерных поверхностей
"""
def surface_painter(data: np.matrix, discrete=40, colormap='coolwarm'):
    m, n = data.shape

    def make_data():
        lat = np.linspace(-math.pi, math.pi, num=n, endpoint=False)
        lon = np.linspace(-math.pi/2, math.pi/2, num=(m+2))[1:-1]
        lat_grid, lon_grid = np.meshgrid(lat, lon)

        return lat_grid, lon_grid, data

    x, y, z = make_data()

    fig = plt.figure()
    ax = Axes3D(fig)
    surf = ax.plot_surface(x, y, z, rstride=1, cstride=1, cmap=plt.cm.get_cmap(colormap, discrete))

    fig.set_size_inches(19.2, 10.8, forward=True)
    fig.colorbar(surf)

    plt.show()

    return
# ======================================================================================================================


info_matrix = read_matrix(filename)

# # x_dist - массив расстояний между соседними точками по широтам
# # y_dist - расстояние между соседними точками по долготам
# x_dist, y_dist = dist_calc(info_matrix.shape[0], info_matrix.shape[1])


"""
    my bilinear
"""
# working_matrix = bilinear_interpolation(info_matrix, new_count=4)

"""
    my bicubic
"""
# working_matrix = bicubic_interpolation(info_matrix, new_count=4)

"""
    my fast bicubic
"""
# working_matrix = fast_bicubic_interpolation(info_matrix, new_count=3)

"""
    my cubic splines
"""
# working_matrix = spline_interpolation(info_matrix, kind='cubic', new_count=4)

"""
    buildin cubic B-slines
"""
# working_matrix = spline_interpolation(info_matrix, kind='cubic_B_splines', new_count=4)

"""
    buildin radial basis function
"""
# working_matrix = spline_interpolation(info_matrix, kind='rbf', new_count=4)

"""
    buildin smooth bivariate splines
"""
# working_matrix = spline_interpolation(info_matrix, kind='smooth_splines', new_count=4)

"""
    my akima splines
"""
# working_matrix = spline_interpolation(info_matrix, kind='akima_splines', new_count=4)

"""
    my bezier surface
"""
# working_matrix = bezier_interpolation(info_matrix, new_count=4)


"""
    Запись проинтерполированной сетки в файл и отрисовка тепловой карты
"""
# head = 'Size: ' + str(working_matrix.shape[0]) + 'x' + str(working_matrix.shape[1])
# np.savetxt('test.out', working_matrix, delimiter='; ', header=head, fmt='%.6f')

# print("--- %s seconds ---" % (time.time() - tic))
# paint_coolwarm_map(working_matrix, discrete=50, colormap='coolwarm')


"""
    Отрисовка трёхмерной поверхности матрицы отличий аппроксимации и оригинальной сетки
"""
# print(calc_difference_matrix(bezier_interpolation(info_matrix, new_count=4), info_matrix).sum() / (info_matrix.shape[0] * info_matrix.shape[1]))
# surface_painter(calc_difference_matrix(spline_interpolation(info_matrix, kind='cubic_B_splines_', new_count=4), info_matrix), discrete=50, colormap='viridis')
# surface_painter(calc_difference_matrix(bezier_interpolation(info_matrix, new_count=4), info_matrix), discrete=50, colormap='viridis')
# surface_painter(calc_mse(spline_interpolation(info_matrix, kind='cubic_B_splines_', new_count=4)[::(2 + 1), ::(2 + 1)], info_matrix), discrete=50, colormap='viridis')
# surface_painter(calc_mse(bezier_interpolation(info_matrix, new_count=4)[::(2 + 1), ::(2 + 1)], info_matrix), discrete=50, colormap='viridis')
# print('cubic_B_splines', calc_mse(spline_interpolation(info_matrix, kind='cubic_B_splines_', new_count=4)[::(2 + 1), ::(2 + 1)], info_matrix).max())
# print('bezier', calc_mse(bezier_interpolation(info_matrix, new_count=4)[::(2 + 1), ::(2 + 1)], info_matrix).max())
# print('cubic_B_splines', calc_mse(spline_interpolation(info_matrix, kind='cubic_B_splines_', new_count=4)[::(2 + 1), ::(2 + 1)], info_matrix).sum() / (47*99))
# print('bezier', calc_mse(bezier_interpolation(info_matrix, new_count=4)[::(2 + 1), ::(2 + 1)], info_matrix).sum() / (47*99))


"""
    Матрица кросс-валидации
"""
# print(cross_validation(info_matrix, 'cubic_splines')[:5, :5])


"""
    Отрисовка трёхмерной поверхности матрицы СКО
"""
# surface_painter(calc_mse(cross_validation(info_matrix, 'bilinear'), info_matrix) / (2**2), discrete=50, colormap='viridis')
# surface_painter(calc_mse(cross_validation(info_matrix, 'bicubic'), info_matrix) / (2**4), discrete=50, colormap='viridis')
# surface_painter(calc_mse(cross_validation(info_matrix, 'cubic_splines'), info_matrix) / (2**4), discrete=50, colormap='viridis')
# surface_painter(calc_mse(cross_validation(info_matrix, 'akima_splines'), info_matrix) / (2**4), discrete=50, colormap='viridis')
# print('bilinear', (calc_mse(cross_validation(info_matrix, 'bilinear'), info_matrix)/(2**2)).sum() / (47*99))
# print('bicubic', (calc_mse(cross_validation(info_matrix, 'bicubic'), info_matrix)/(2**4)).sum() / (47*99))
# print('cubic_splines', (calc_mse(cross_validation(info_matrix, 'cubic_splines'), info_matrix)/(2**4)).sum() / (47*99))
# print('akima_splines', (calc_mse(cross_validation(info_matrix, 'akima_splines'), info_matrix)/(2**4)).sum() / (47*99))
# print('bilinear', (calc_mse(cross_validation(info_matrix, 'bilinear'), info_matrix).max() /(2**2)))
# print('bicubic', (calc_mse(cross_validation(info_matrix, 'bicubic'), info_matrix).max() /(2**4)))
# print('cubic_splines', (calc_mse(cross_validation(info_matrix, 'cubic_splines'), info_matrix).max() /(2**4)))
# print('akima_splines', (calc_mse(cross_validation(info_matrix, 'akima_splines'), info_matrix).max() /(2**4)))


"""
    Вычисляем матрицу значений в дельта окрестности
"""
# surface_painter(calc_mse(derivative_limit(info_matrix, delta=1, kind='bilinear'), info_matrix), discrete=50, colormap='viridis')
# surface_painter(calc_mse(derivative_limit(info_matrix, delta=1, kind='bicubic'), info_matrix), discrete=50, colormap='viridis')
# surface_painter(calc_mse(derivative_limit(info_matrix, delta=1, kind='cubic_splines'), info_matrix), discrete=50, colormap='viridis')
# surface_painter(calc_mse(derivative_limit(info_matrix, delta=1, kind='cubic_B_splines'), info_matrix), discrete=50, colormap='viridis')
# # surface_painter(calc_mse(derivative_limit(info_matrix, delta=1, kind='rbf'), info_matrix), discrete=50, colormap='viridis')
# # surface_painter(calc_mse(derivative_limit(info_matrix, delta=1, kind='smooth_splines'), info_matrix), discrete=50, colormap='viridis')
# surface_painter(calc_mse(derivative_limit(info_matrix, delta=1, kind='bezier'), bezier_interpolation(info_matrix, new_count=2)), discrete=50, colormap='viridis')


"""
    Типа проверка условия на ограничение производной
"""
# print('bilinear', calc_mse(derivative_limit(info_matrix, delta=1, kind='bilinear'), info_matrix)[1])
# print('bicubic', calc_mse(derivative_limit(info_matrix, delta=1, kind='bicubic'), info_matrix)[1])
# print('cubic_splines', calc_mse(derivative_limit(info_matrix, delta=1, kind='cubic_splines'), info_matrix)[1])
# print('akima_splines', calc_mse(derivative_limit(info_matrix, delta=1, kind='akima_splines'), info_matrix)[1])
# print('cubic_B_splines', calc_mse(derivative_limit(info_matrix, delta=1, kind='cubic_B_splines'), info_matrix)[1])
# print('bezier', calc_mse(derivative_limit(info_matrix, delta=1, kind='bezier'), bezier_interpolation(info_matrix, new_count=2))[1])

# def calc_mse(interp: np.matrix, orig: np.matrix) -> np.matrix:
    # m, n = orig.shape
    # res = np.zeros(orig.shape)
    # mx = -10000
 
    ##перебор по строкам
    # for i in range(m):
        ##перебор по столбцам
        # for j in range(n):
            # if type(interp[i, j]) is np.float64:
                # res[i, j] = round(((interp[i, j] - orig[i, j]) ** 2), 6)
                # if (interp[i, j] - orig[i, j]) > mx:
                    # mx = (interp[i, j] - orig[i, j])
            # elif interp[i, j] is not None:
                # res[i, j] = round((((interp[i, j] - orig[i, j]) ** 2).sum() / len(interp[i, j])), 6)
                # if (interp[i, j] - orig[i, j]).max() > mx:
                    # mx = (interp[i, j] - orig[i, j]).max()
            # else:
                # res[i, j] = 0

    # return res, mx
