void meteo_proj_grid_t::render( meteo_proj_grid_t&                grid_,//сетка - считанные из файла данные
                                QPainter&                         painter_, //для рисования
                                const icegis::Prj2DevTransformer& prj2dev_,//не обращай внимания - для отображения на карте
                                ice_map_guard_t&                  guard_map_,//не обращай внимания
                                const uint64_t                    type_ )//тип изолиний - для температуры, давления и т.п. (тебе это тоже не важно)
{

  QPen pen;
  pen.setWidth( 3 );
  painter_.setPen( pen );

 /* if( grid_.coord_grid.empty() )
    grid_.updateCoordGrid( guard_map_ );
  if( !grid_.geo_coord_grid.hasHeithgGrid(type_) )
    return;
*/
  std::pair< uint16_t, uint16_t > sz = grid_.geo_coord_grid.latLonSize();// sz - размер матрицы из файла


  ///
  std::vector< double > heights;//вектор температур - шаг через 4 градуса от -104 до 100 градусов
  uint64_t shift = 25;//0 градусов будет на этой позиции в векторе температур
  const double step = 4.;
  {
    double h = -104.;
    
    do
    {
      h += step;
      heights.push_back( h );
    }
    while( h < 100 );
  }
  ///

  
  enum hand_t// обозначение для сторон квадрата
  {
    left_s,
    top_s,
    right_s,
    bottom,
  };


  const std::shared_ptr< const std::vector< std::vector< double > > > height_grid = grid_.geo_coord_grid.heightGrid( type_ );//данные из файла

  auto left_ind = [ &step, &shift ] ( const double h_ )//получение индекса температуры в массиве слева от заданного
  {
    const double quotient = h_ / step + shift;
    return (uint64_t)quotient;
  };


  auto hands_with_h = [] ( const std::vector< double > sh_, const double h_ )//получить стороны квадрата, где будет проходить изолиния
  {
    std::vector< hand_t > res;

    if( ((std::min)(bl_h(sh_), tl_h(sh_)) < h_) && (h_ < (std::max)(bl_h(sh_), tl_h(sh_))) )
      res.push_back( left_s );

    if( ((std::min)(tl_h(sh_), tr_h(sh_)) < h_) && (h_ < (std::max)(tl_h(sh_), tr_h(sh_))) )
      res.push_back( top_s );

    if( ((std::min)(tr_h(sh_), br_h(sh_)) < h_) && (h_ < (std::max)(tr_h(sh_), br_h(sh_))) )
      res.push_back( right_s );

    if( ((std::min)(br_h(sh_), bl_h(sh_)) < h_) && (h_ < (std::max)(br_h(sh_), bl_h(sh_))) )
      res.push_back( bottom );

    assert( (res.size() == 2) || (res.size() == 4) );

    return res;
  };

  std::vector< std::vector< TPLDCoord > >& cg_ = grid_.coord_grid;//здесь тебе нужно будет записать экранные координаты точек (QPoint в Qt)
  
  //ищем, в какой точке изолиния пересекает сторону квадрата
  auto calcPointByH = [ &cg_ ] ( const uint64_t i_, 
                                 const uint64_t j_, 
                                 hand_t hand_, 
                                 const std::vector< double >& sh_,
                                 const double h_ )
  {
    switch( hand_ )
    {
    case left_s:
      return _calcPointByH( cg_.at(i_).at(j_), cg_.at(i_+1).at(j_), bl_h(sh_), tl_h(sh_), h_ );//i - по вертикальной оси, j - по горизонтальной

    case top_s:
      return _calcPointByH( cg_.at(i_+1).at(j_), cg_.at(i_+1).at(j_+1), tl_h(sh_), tr_h(sh_), h_ );

    case right_s:
      return _calcPointByH( cg_.at(i_+1).at(j_+1), cg_.at(i_).at(j_+1), tr_h(sh_), br_h(sh_), h_ );

    case bottom:
      return _calcPointByH( cg_.at(i_).at(j_), cg_.at(i_).at(j_+1), bl_h(sh_), br_h(sh_), h_ );

    default:
      assert( false );
    }


    return TPLDCoord();
  };


  QPainterPath path;


  //шагаем по квадратам
  for( uint64_t i = 0; i < grid_.coord_grid.size() - 1; ++i )
  {
    const std::vector< TPLDCoord >& bot_coord_row = grid_.coord_grid.at(i),
                                  & top_coord_row = grid_.coord_grid.at(i+1);

    const std::vector< double >& bot_height_row = height_grid->at(i),
                               & top_height_row = height_grid->at(i+1);

    for( uint64_t j = 0, h_j = 0, h1_j = 1; j < bot_coord_row.size() - 1; ++j, ++h_j, ++h1_j )
    {

      if( h_j == bot_height_row.size() )
        h_j -=  bot_height_row.size();
      if( h1_j ==  bot_height_row.size() )
        h1_j -=  bot_height_row.size();

      
	//температура в углах квадрата	
      const std::vector< double > sh = { bot_height_row.at(h_j), top_height_row.at(h_j), top_height_row.at(h1_j), bot_height_row.at(h1_j) };


      const double min_h = (std::min)( (std::min)(bl_h(sh), tl_h(sh)), (std::min)(tr_h(sh), br_h(sh)) ),//находим в квадрате max и min температуру (в углах)
                   max_h = (std::max)( (std::max)(bl_h(sh), tl_h(sh)), (std::max)(tr_h(sh), br_h(sh)) );

      const uint64_t lind = left_ind( min_h ),
                     rind = left_ind( max_h ) + 1;
      

      assert( min_h > heights.at(lind) );
      assert( min_h < heights.at(lind + 1) );

      assert( max_h < heights.at(rind) );
      assert( max_h > heights.at(rind - 1) );

      if( lind == rind - 1 )
        continue;

	//определяем, какие температуры, кратные 4 градусам попадут в квадрат
      std::vector< double > h_in_square;
      for( uint64_t i = lind + 1; i < rind; ++i )
        h_in_square.push_back( heights.at(i) );


		//цикл по найденным температурам  + отрисовка изолиний
      for( const auto h: h_in_square )
      {
        std::vector< hand_t > hands = hands_with_h( sh, h );


        if( hands.size() == 2 )
        {
          const TPLDCoord pt1 = calcPointByH( i, j, hands.front(), sh, h ),
                          pt2 = calcPointByH( i, j, hands.back(), sh, h );

          path.moveTo( fromProjToScreenPoint( pt1, prj2dev_ ) );
          path.lineTo( fromProjToScreenPoint( pt2, prj2dev_ ) );
        }
        else
        {
          TPLDCoord pt1, pt2, pt3, pt4;

          {
            pt1 = calcPointByH( i, j, left_s, sh, h ),
            pt2 = tl_h( sh ) > h ? calcPointByH( i, j, bottom, sh, h ) : calcPointByH( i, j, top_s, sh, h ),

            pt4 = calcPointByH( i, j, right_s, sh, h ),
            pt3 = br_h( sh ) > h ? calcPointByH( i, j, top_s, sh, h ) : calcPointByH( i, j, bottom, sh, h );
          }
                          

          path.moveTo( fromProjToScreenPoint( pt1, prj2dev_ ) );
          path.lineTo( fromProjToScreenPoint( pt2, prj2dev_ ) );

          path.moveTo( fromProjToScreenPoint( pt3, prj2dev_ ) );
          path.lineTo( fromProjToScreenPoint( pt4, prj2dev_ ) );
        }
      }
    }
  }


  painter_.drawPath( path );
}